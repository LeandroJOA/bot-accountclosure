require('dotenv').config()

const { readSheet, writeSheet, getToken } = require('./accountClosure');

async function main() {

    console.log('-----> Starting BOT');

    const { workbook, xlData } = readSheet(process.env.FILENAME);

    const tokenJWT = await getToken();

    await writeSheet(workbook, xlData, tokenJWT);

    console.log('-----> Finishing BOT');
}

main()











