const xlsx = require('xlsx');
const axios = require('axios').default;
const moment = require("moment");

function readSheet(fileName) {

    console.log('-> Opening XLSX file');

    const workbook = xlsx.readFile(`data/${fileName}`),
        sheet_name_list = workbook.SheetNames,
        xlData = xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);

    return {
        workbook,
        xlData,
    };
}

async function writeSheet(file, data, tokenJWT) {

    for (const row of data) {
        console.log('---------------------------------------');
        console.log('Client: ' + row.cpf);

        const id = await getAccount(row.cpf, tokenJWT);
        const res = await accountClosure(id, tokenJWT);

        row.DT_FIM = res.DT_FIM;
        row.statusSparks = res.statusSparks;
        row.statusBV = res.statusBV;
        row.motivoBloqueio = res.motivoBloqueio;
    }

    const ws = xlsx.utils.json_to_sheet(data);

    xlsx.utils.book_append_sheet(file, ws, "NEW BASE IPIRANGA");

    xlsx.writeFile(file, './data/teste.xlsx');
}

async function getAccount(documentNumber, tokenJWT) {

    console.log('-> Getting AccountID');

    const body = {
        documentNumber,
    };

    const header = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': tokenJWT,
        },
    };

    try {
        const { data } = await axios.post(`${process.env.ENDPOINT}accounts/verify-document`, body, header);

        console.log('AccountID: ' + data.accountId);

        return data.accountId;
    } catch (error) {
        if (error.response.data.status > 499 && error.response.data.status < 600) {
            console.log(error.response.data);
            throw error;
        }

        console.log(error.response.data);
    }
}

async function getToken() {

    console.log('-> Getting TokenJWT');

    const header = {
        headers: {
            'Content-Type': 'application/json',
            'API_KEY': process.env.API_KEY,
        },
    };

    try {
        const { data } = await axios.get(`${process.env.ENDPOINT}authorizers/jwt-token`, header);

        console.log('Token: ' + data.token);

        return data.token;
    } catch (error) {
        if (error.response.data.status > 499 && error.response.data.status < 600) {
            console.log(error.response.data);
            throw error;
        }

        console.log(error.response.data);
    }
}

async function accountClosure(accountId, tokenJWT) {

    const body = {
        //reason: 'Teste',
        //actorId: '00ad6952-5717-4330-b663-b3cd9f02f489'
    };

    const header = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': tokenJWT,
        },
    };

    try {
        const { data } = await axios.post(`${process.env.ENDPOINT}accounts/account-closure/${accountId}`, body, header);

        if (data.pixKey || data.balance) {
            const res = await accountBlock(accountId, tokenJWT);
            const motivo = res == 'Conta bloqueada' ? data.message : '';

            console.log('Status: ' + res);

            return {
                DT_FIM: null,
                statusSparks: res,
                statusBV: null,
                motivoBloqueio: motivo,
            };
        } else {
            console.log('-> Closing Account');

            const statusSparks = data.statusOnSparks == 'An account was deleted' ? 'Conta encerrada' : data.statusOnSparks.message;

            const statusBV = data.statusOnBV.statusCadastro == 'OK' ? 'Conta encerrada' : data.statusOnBV.descricaoMensagemRetorno;

            const DT_FIM = statusSparks == 'Conta encerrada' && statusBV == 'Conta encerrada' ? moment().format('YYYY-MM-DD') : '';

            console.log('Status Sparks: ' + data.statusOnSparks.message || data.statusOnSparks);
            console.log('Status BV: ' + data.statusOnBV.statusCadastro);

            return {
                DT_FIM,
                statusSparks,
                statusBV,
                motivoBloqueio: null,
            };
        }
    } catch (error) {
        if (error.response.data.status == 404) {
            return {
                statusSparks: 'AccountID invalido',
                statusBV: 'AccountID invalido',
            };
        } else if (error.response.data.status > 499 && error.response.data.status < 600) {
            console.log(error.response.data);
            throw error;
        }

        console.log(error.response.data);
    }
}

async function accountBlock(accountId, tokenJWT) {

    console.log('-> Blocking Account');

    const body = {};

    const header = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': tokenJWT,
        },
    };

    try {
        const { data } = await axios.post(`${process.env.ENDPOINT}accounts/${accountId}/block`, body, header);

        return data.msg == 'Account blocked with success.' ? 'Conta bloqueada' : '';
    } catch (error) {
        if (error.response.data.status == 422) {
            return error.response.data.message;
        } else if (error.response.data.status > 499 && error.response.data.status < 600) {
            console.log(error.response.data);
            throw error;
        }

        console.log(error.response.data);
    }
}

module.exports = {
    readSheet,
    writeSheet,
    getToken,
};